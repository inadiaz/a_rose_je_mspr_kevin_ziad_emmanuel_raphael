import 'package:a_rosa_je/view/profil_page.dart';

class ProfilViewModel extends IProfilePageViewModel {
  //private prop
  int? id = 1;
  String? _pseudo = "Geralt";
  String? _role = "Gardien";
  String? _firstName = "Geralt";
  String? _lastName = "De Riv";
  int? _phone = 0688552244;
  String? _email = "loupblanc@chasseur.witcher";
  String? _address = " Foret de WestWood";
  String? _image;
  //end private prop

  //Getter

  String? get pseudo => _pseudo;
  String? get role => _role;
  String? get firstName => _firstName;
  String? get email => _email;
  String? get image => _image;
  String? get lastName => _lastName;
  int? get phone => _phone;
  String? get address => _address;
//End Getter

}
