import 'package:flutter/material.dart';
import 'view/home_page.dart';
import 'viewModel/home_view_model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  ThemeData _buildCustomTheme() {
    final baseTheme = ThemeData.light();
    final colorScheme =
        ColorScheme.fromSeed(seedColor: const Color.fromARGB(255, 10, 109, 63));
    return baseTheme.copyWith(colorScheme: colorScheme);
  }

  ThemeData _buildCustomDarkTheme() {
    final base = ThemeData.dark();
    final colorScheme = ColorScheme.fromSeed(
        seedColor: const Color.fromARGB(255, 121, 216, 44));
    return base.copyWith(colorScheme: colorScheme);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'app',
      theme: _buildCustomTheme(),
      darkTheme: _buildCustomDarkTheme(),
      home: HomePage(HomeViewModel()),
    );
  }
}
