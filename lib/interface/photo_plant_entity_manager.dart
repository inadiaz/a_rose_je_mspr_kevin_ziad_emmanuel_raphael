import 'dart:async';
import 'dart:io' as Io;
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'package:a_rosa_je/interface/AppDatabase.dart';
import '../model/photo_plant.dart';

class PhotoPlantDataManager {
  Future<PhotoPlantDao> get _photoPlantDao async {
    final database =
        await $FloorAppDatabase.databaseBuilder('app_database4.db').build();

    return database.photoPlantDao;
  }

  Future<void> addNewPhotoPlant(PhotoPlant photoPlant) async {
    final photoPlantDao = await _photoPlantDao;
    return photoPlantDao.insertPhotoPlant(TPhotoPlant.fromMap(photoPlant));
  }

  Future<void> updatePhotoPlantToLocal(PhotoPlant photoPlant) async {
    final photoPlantDao = await _photoPlantDao;
    photoPlantDao.updatePhotoPlant(
        photoPlant.plantId, photoPlant.photoId, photoPlant.id!);
  }

  Future<void> deleteCurrentPhotoPlant(PhotoPlant photoPlant) async {
    final photoPlantDao = await _photoPlantDao;
    photoPlantDao.deletePhotoPlant(photoPlant.id!);
  }

  Future<PhotoPlant?> getSinglePhotoPlantById(int id) async {
    final photoPlantDao = await _photoPlantDao;
    final tPhotoPlant = await photoPlantDao.findPhotoPlantById(id);
    return tPhotoPlant?.photoPlant;
  }

  Future<List<PhotoPlant>> getPhotoPlantList() async {
    final photoPlantDao = await _photoPlantDao;
    final tPhotoPlantList = await photoPlantDao.findAllPhotoPlants();
    return tPhotoPlantList.map((pp) => pp.photoPlant).toList();
  }
}

@dao
abstract class PhotoPlantDao {
  @Query('SELECT * from TPhotoPlant')
  Future<List<TPhotoPlant>> findAllPhotoPlants();

  @Query('SELECT * from TPhotoPlant WHERE id = :id')
  Future<TPhotoPlant?> findPhotoPlantById(int id);

  @Query('DELETE from TPhotoPlant WHERE id = :id')
  Future<TPhotoPlant?> deletePhotoPlant(int id);

  @insert
  Future<void> insertPhotoPlant(TPhotoPlant photoPlant);

  @Query(
      'UPDATE TPhotoPlant SET plantId = :plantId, photoId = :photoId WHERE id = :id')
  Future<void> updatePhotoPlant(int plantId, int photoId, int id);
}

@entity
class TPhotoPlant {
  @PrimaryKey(autoGenerate: true)
  int? id;
  int plantId;
  int photoId;

  TPhotoPlant(this.id, this.plantId, this.photoId);

  TPhotoPlant.fromMap(PhotoPlant photoPlant)
      : id = photoPlant.id,
        plantId = photoPlant.plantId,
        photoId = photoPlant.photoId;

  PhotoPlant get photoPlant => PhotoPlant(id, plantId, photoId);
}
