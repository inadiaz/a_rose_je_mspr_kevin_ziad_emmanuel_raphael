import 'dart:async';
import 'dart:io' as Io;
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'package:a_rosa_je/interface/AppDatabase.dart';
import '../model/user_role.dart';

class UserRoleDataManager {
  Future<UserRoleDao> get _userRoleDao async {
    final database =
        await $FloorAppDatabase.databaseBuilder('app_database4.db').build();

    return database.userRoleDao;
  }

  Future<void> addNewUserRole(UserRole userRole) async {
    final userRoleDao = await _userRoleDao;
    return userRoleDao.insertUserRole(TUserRole.fromMap(userRole));
  }

  Future<void> updateUserRoleToLocal(UserRole userRole) async {
    final userRoleDao = await _userRoleDao;
    userRoleDao.updateUserRole(userRole.type, userRole.id!);
  }

  Future<void> deleteCurrentUserRole(UserRole userRole) async {
    final userRoleDao = await _userRoleDao;
    userRoleDao.deleteUserRole(userRole.id!);
  }

  Future<UserRole?> getSingleUserRoleById(int id) async {
    final userRoleDao = await _userRoleDao;
    final tUserRole = await userRoleDao.findUserRoleById(id);
    return tUserRole?.userRole;
  }

  Future<List<UserRole>> getUserRoleList() async {
    final userRoleDao = await _userRoleDao;
    final tUserRoleList = await userRoleDao.findAllUserRoles();
    return tUserRoleList.map((ur) => ur.userRole).toList();
  }
}

@dao
abstract class UserRoleDao {
  @Query('SELECT * from TUserRole')
  Future<List<TUserRole>> findAllUserRoles();

  @Query('SELECT * from TUserRole WHERE id = :id')
  Future<TUserRole?> findUserRoleById(int id);

  @Query('DELETE from TUserRole WHERE id = :id')
  Future<TUserRole?> deleteUserRole(int id);

  @insert
  Future<void> insertUserRole(TUserRole userRole);

  @Query('UPDATE TUserRole SET type = :type WHERE id = :id')
  Future<void> updateUserRole(String type, int id);
}

@entity
class TUserRole {
  @PrimaryKey(autoGenerate: true)
  int? id;
  String type;

  TUserRole(this.id, this.type);

  TUserRole.fromMap(UserRole userRole)
      : id = userRole.id,
        type = userRole.type;

  UserRole get userRole => UserRole(id, type);
}
