import 'dart:async';
import '../model/advice.dart';
import 'dart:io' as Io;
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'AppDatabase.dart';

class AdviceDataManager {
  Future<AdviceDao> get _adviceDao async {
    final database =
        await $FloorAppDatabase.databaseBuilder('app_database4.db').build();
    return database.adviceDao;
  }

  Future<void> addNewAdvice(Advice advice) async {
    final adviceDao = await _adviceDao;
    return adviceDao.insertAdvice(TAdvice.fromMap(advice));
  }

  Future<void> updateAdviceToLocal(Advice advice) async {
    final adviceDao = await _adviceDao;
    adviceDao.updateAdvice(advice.title, advice.comment,
        advice.publishDate.microsecondsSinceEpoch, advice.id!);
  }

  Future<void> deleteCurrentAdvice(Advice advice) async {
    final adviceDao = await _adviceDao;
    adviceDao.deleteAdvice(advice.id!);
  }

  Future<Advice?> getSingleAdviceByTitle(String title) async {
    final adviceDao = await _adviceDao;
    final tAdvice = await adviceDao.findAdviceByTitle(title);
    return tAdvice?.advice;
  }

  Future<Advice?> getSingleAdviceById(int id) async {
    final adviceDao = await _adviceDao;
    final tAdvice = await adviceDao.findAdviceById(id);
    return tAdvice?.advice;
  }

  Future<List<Advice>> getAdviceList() async {
    final adviceDao = await _adviceDao;
    final tAdviceList = await adviceDao.findAllAdvices();
    return tAdviceList.map((a) => a.advice).toList();
  }
}

@dao
abstract class AdviceDao {
  @Query('SELECT * from TAdvice')
  Future<List<TAdvice>> findAllAdvices();

  @Query('SELECT * from TAdvice WHERE id = :id')
  Future<TAdvice?> findAdviceById(int id);

  @Query('SELECT * from TAdvice WHERE title = :title')
  Future<TAdvice?> findAdviceByTitle(String title);

  @insert
  Future<void> insertAdvice(TAdvice advice);

  @Query(
      'UPDATE TAdvice SET title = :title, comment = :comment, publishDate = :publishDate WHERE id = :id')
  Future<void> updateAdvice(
      String title, String comment, int publishDate, int id);

  @Query('DELETE from TAdvice WHERE id = :id')
  Future<TAdvice?> deleteAdvice(int id);
}

@entity
class TAdvice {
  @PrimaryKey(autoGenerate: true)
  int? id;
  String title;
  String comment;
  int publishDate;

  TAdvice(this.id, this.title, this.comment, this.publishDate);

  TAdvice.fromMap(Advice advice)
      : id = advice.id,
        title = advice.title,
        comment = advice.comment,
        publishDate = advice.publishDate.microsecondsSinceEpoch;

  Advice get advice => Advice(
      id, title, comment, DateTime.fromMicrosecondsSinceEpoch(publishDate));
}
