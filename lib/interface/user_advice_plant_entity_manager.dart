import 'dart:async';
import 'dart:io' as Io;
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'package:a_rosa_je/interface/AppDatabase.dart';
import '../model/user_advice_plant.dart';

class UserAdvicePlantDataManager {
  Future<UserAdvicePlantDao> get _userAdvicePlantDao async {
    final database =
        await $FloorAppDatabase.databaseBuilder('app_database4.db').build();

    return database.userAdvicePlantDao;
  }

  Future<void> addNewUserAdvicePlant(UserAdvicePlant userAdvicePlant) async {
    final userAdvicePlantDao = await _userAdvicePlantDao;
    return userAdvicePlantDao
        .insertUserAdvicePlant(TUserAdvicePlant.fromMap(userAdvicePlant));
  }

  Future<void> updateUserAdvicePlantToLocal(
      UserAdvicePlant userAdvicePlant) async {
    final userAdvicePlantDao = await _userAdvicePlantDao;
    userAdvicePlantDao.updateUserAdvicePlant(userAdvicePlant.userId,
        userAdvicePlant.adviceId, userAdvicePlant.plantId, userAdvicePlant.id!);
  }

  Future<void> deleteCurrentUserAdvicePlant(
      UserAdvicePlant userAdvicePlant) async {
    final userAdvicePlantDao = await _userAdvicePlantDao;
    userAdvicePlantDao.deleteUserAdvicePlant(userAdvicePlant.id!);
  }

  Future<UserAdvicePlant?> getSingleUserAdvicePlantById(int id) async {
    final userAdvicePlantDao = await _userAdvicePlantDao;
    final tUserAdvicePlant =
        await userAdvicePlantDao.findUserAdvicePlantById(id);
    return tUserAdvicePlant?.userAdvicePlant;
  }

  Future<List<UserAdvicePlant>> getUserAdvicePlantList() async {
    final userAdvicePlantDao = await _userAdvicePlantDao;
    final tUserAdvicePlantList =
        await userAdvicePlantDao.findAllUserAdvicePlants();
    return tUserAdvicePlantList.map((uap) => uap.userAdvicePlant).toList();
  }
}

@dao
abstract class UserAdvicePlantDao {
  @Query('SELECT * from TUserAdvicePlant')
  Future<List<TUserAdvicePlant>> findAllUserAdvicePlants();

  @Query('SELECT * from TUserAdvicePlant WHERE id = :id')
  Future<TUserAdvicePlant?> findUserAdvicePlantById(int id);

  @Query('DELETE from TUserAdvicePlant WHERE id = :id')
  Future<TUserAdvicePlant?> deleteUserAdvicePlant(int id);

  @insert
  Future<void> insertUserAdvicePlant(TUserAdvicePlant userAdvicePlant);

  @Query(
      'UPDATE TUserAdvicePlant SET userId = :userId, adviceId = :adviceId, plantId = :plantId WHERE id = :id')
  Future<void> updateUserAdvicePlant(
      int userId, int adviceId, int plantId, int id);
}

@entity
class TUserAdvicePlant {
  @PrimaryKey(autoGenerate: true)
  int? id;
  int userId;
  int adviceId;
  int plantId;

  TUserAdvicePlant(this.id, this.userId, this.adviceId, this.plantId);

  TUserAdvicePlant.fromMap(UserAdvicePlant userAdvicePlant)
      : id = userAdvicePlant.id,
        userId = userAdvicePlant.userId,
        adviceId = userAdvicePlant.adviceId,
        plantId = userAdvicePlant.plantId;

  UserAdvicePlant get userAdvicePlant =>
      UserAdvicePlant(id, userId, adviceId, plantId);
}
