import 'dart:async';
import 'dart:io' as Io;
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'package:a_rosa_je/interface/AppDatabase.dart';
import '../model/plant_announce.dart';

class PlantAnnounceDataManager {
  Future<PlantAnnounceDao> get _plantAnnounceDao async {
    final database =
        await $FloorAppDatabase.databaseBuilder('app_database4.db').build();

    return database.plantAnnounceDao;
  }

  Future<void> addNewPlantAnnounce(PlantAnnounce plantAnnounce) async {
    final plantAnnounceDao = await _plantAnnounceDao;
    return plantAnnounceDao
        .insertPlantAnnounce(TPlantAnnounce.fromMap(plantAnnounce));
  }

  Future<void> updatePlantAnnounceToLocal(PlantAnnounce plantAnnounce) async {
    final plantAnnounceDao = await _plantAnnounceDao;
    plantAnnounceDao.updatePlantAnnounce(
        plantAnnounce.plantId, plantAnnounce.announceId, plantAnnounce.id!);
  }

  Future<void> deleteCurrentPlantAnnounce(PlantAnnounce plantAnnounce) async {
    final plantAnnounceDao = await _plantAnnounceDao;
    plantAnnounceDao.deletePlantAnnounce(plantAnnounce.id!);
  }

  Future<PlantAnnounce?> getSinglePlantAnnounceById(int id) async {
    final plantAnnounceDao = await _plantAnnounceDao;
    final tPlantAnnounce = await plantAnnounceDao.findPlantAnnounceById(id);
    return tPlantAnnounce?.plantAnnounce;
  }

  Future<List<PlantAnnounce>> getPlantAnnounceList() async {
    final plantAnnounceDao = await _plantAnnounceDao;
    final tPlantAnnounceList = await plantAnnounceDao.findAllPlantAnnounces();
    return tPlantAnnounceList.map((pa) => pa.plantAnnounce).toList();
  }
}

@dao
abstract class PlantAnnounceDao {
  @Query('SELECT * from TPlantAnnounce')
  Future<List<TPlantAnnounce>> findAllPlantAnnounces();

  @Query('SELECT * from TPlantAnnounce WHERE id = :id')
  Future<TPlantAnnounce?> findPlantAnnounceById(int id);

  @Query('DELETE from TPlantAnnounce WHERE id = :id')
  Future<TPlantAnnounce?> deletePlantAnnounce(int id);

  @insert
  Future<void> insertPlantAnnounce(TPlantAnnounce plantAnnounce);

  @Query(
      'UPDATE TPlantAnnounce SET plantId = :plantId, announceId = :announceId WHERE id = :id')
  Future<void> updatePlantAnnounce(int plantId, int announceId, int id);
}

@entity
class TPlantAnnounce {
  @PrimaryKey(autoGenerate: true)
  int? id;
  int plantId;
  int announceId;

  TPlantAnnounce(this.id, this.plantId, this.announceId);

  TPlantAnnounce.fromMap(PlantAnnounce plantAnnounce)
      : id = plantAnnounce.id,
        plantId = plantAnnounce.plantId,
        announceId = plantAnnounce.announceId;

  PlantAnnounce get plantAnnounce => PlantAnnounce(id, plantId, announceId);
}
