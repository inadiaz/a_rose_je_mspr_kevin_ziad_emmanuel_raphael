import 'dart:async';
import 'dart:io' as Io;
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'package:a_rosa_je/interface/AppDatabase.dart';
import '../model/user_announce.dart';

class UserAnnounceDataManager {
  Future<UserAnnounceDao> get _userAnnounceDao async {
    final database =
        await $FloorAppDatabase.databaseBuilder('app_database4.db').build();

    return database.userAnnounceDao;
  }

  Future<void> addNewUserAnnounce(UserAnnounce userAnnounce) async {
    final userAnnounceDao = await _userAnnounceDao;
    return userAnnounceDao
        .insertUserAnnounce(TUserAnnounce.fromMap(userAnnounce));
  }

  Future<void> updateUserAnnounceToLocal(UserAnnounce userAnnounce) async {
    final userAnnounceDao = await _userAnnounceDao;
    userAnnounceDao.updateUserAnnounce(userAnnounce.keeperId,
        userAnnounce.ownerId, userAnnounce.announceId, userAnnounce.id!);
  }

  Future<void> deleteCurrentUserAnnounce(UserAnnounce userAnnounce) async {
    final userAnnounceDao = await _userAnnounceDao;
    userAnnounceDao.deleteUserAnnounce(userAnnounce.id!);
  }

  Future<UserAnnounce?> getSingleUserAnnounceById(int id) async {
    final userAnnounceDao = await _userAnnounceDao;
    final tUserAnnounce = await userAnnounceDao.findUserAnnounceById(id);
    return tUserAnnounce?.userAnnounce;
  }

  Future<List<UserAnnounce>> getUserAnnounceList() async {
    final userAnnounceDao = await _userAnnounceDao;
    final tUserAnnounceList = await userAnnounceDao.findAllUserAnnounces();
    return tUserAnnounceList.map((ua) => ua.userAnnounce).toList();
  }
}

@dao
abstract class UserAnnounceDao {
  @Query('SELECT * from TUserAnnounce')
  Future<List<TUserAnnounce>> findAllUserAnnounces();

  @Query('SELECT * from TUserAnnounce WHERE id = :id')
  Future<TUserAnnounce?> findUserAnnounceById(int id);

  @Query('DELETE from TUserAnnounce WHERE id = :id')
  Future<TUserAnnounce?> deleteUserAnnounce(int id);

  @insert
  Future<void> insertUserAnnounce(TUserAnnounce userAnnounce);

  @Query(
      'UPDATE TUserAnnounce SET keeperId = :keeperId, ownerdId = :ownerId, announceId = :announceId WHERE id = :id')
  Future<void> updateUserAnnounce(
      int keeperId, int ownerId, int announceId, int id);
}

@entity
class TUserAnnounce {
  @PrimaryKey(autoGenerate: true)
  int? id;
  int keeperId;
  int ownerdId;
  int annouceId;

  TUserAnnounce(this.id, this.keeperId, this.ownerdId, this.annouceId);

  TUserAnnounce.fromMap(UserAnnounce userAnnounce)
      : id = userAnnounce.id,
        keeperId = userAnnounce.keeperId,
        ownerdId = userAnnounce.ownerId,
        annouceId = userAnnounce.announceId;

  UserAnnounce get userAnnounce =>
      UserAnnounce(id, keeperId, ownerdId, annouceId);
}
