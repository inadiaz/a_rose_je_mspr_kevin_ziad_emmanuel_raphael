import 'dart:async';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'dart:io' as Io;

import 'package:a_rosa_je/interface/advice_entity_manager.dart';
import 'package:a_rosa_je/interface/announce_entity_manager.dart';
import 'package:a_rosa_je/interface/plant_entity_manager.dart';
import 'package:a_rosa_je/interface/plant_announce_entity_manager.dart';
import 'package:a_rosa_je/interface/photo_plant_entity_manager.dart';
import 'package:a_rosa_je/interface/user_entity_manager.dart';
import 'package:a_rosa_je/interface/user_advice_plant_entity_manager.dart';
import 'package:a_rosa_je/interface/user_announce_entity_manager.dart';
import 'package:a_rosa_je/interface/user_role_entity_manager.dart';

part 'AppDatabase.g.dart';

@Database(version: 1, entities: [
  TAdvice,
  TAnnounce,
  TPlant,
  TPlantAnnounce,
  TPhotoPlant,
  TUser,
  TUserAdvicePlant,
  TUserAnnounce,
  TUserRole
])
abstract class AppDatabase extends FloorDatabase {
  AdviceDao get adviceDao;
  AnnounceDao get announceDao;
  PlantDao get plantDao;
  PlantAnnounceDao get plantAnnounceDao;
  PhotoPlantDao get photoPlantDao;
  UserDao get userDao;
  UserAdvicePlantDao get userAdvicePlantDao;
  UserAnnounceDao get userAnnounceDao;
  UserRoleDao get userRoleDao;
}
