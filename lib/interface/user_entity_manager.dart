import 'dart:async';
import 'package:a_rosa_je/interface/AppDatabase.dart';
import '../model/user.dart';
import 'dart:io' as Io;
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

class UserDataManager {
  Future<UserDao> get _userDao async {
    final database =
        await $FloorAppDatabase.databaseBuilder('app_database4.db').build();

    return database.userDao;
  }

  Future<void> addNewUser(User user) async {
    final userDao = await _userDao;
    return userDao.insertUser(TUser.fromMap(user));
  }

  Future<void> updateUserToLocal(User user) async {
    final userDao = await _userDao;
    userDao.updateUser(user.login, user.firstName, user.lastName, user.email,
        user.address, user.phone, user.id!);
  }

  Future<void> deleteCurrentUser(User user) async {
    final userDao = await _userDao;
    userDao.deleteUser(user.id!);
  }

  Future<User?> getSingleUserByName(String name) async {
    final userDao = await _userDao;
    final tUser = await userDao.findUserByName(name);
    return tUser?.user;
  }

  Future<User?> getSingleUserById(int id) async {
    final userDao = await _userDao;
    final tUser = await userDao.findUserById(id);
    return tUser?.user;
  }

  Future<List<User>> getUserList() async {
    final userDao = await _userDao;
    final tUserList = await userDao.findAllUsers();
    return tUserList.map((u) => u.user).toList();
  }
}

@dao
abstract class UserDao {
  @Query('SELECT * from TUser')
  Future<List<TUser>> findAllUsers();

  @Query('SELECT * from TUser WHERE id = :id')
  Future<TUser?> findUserById(int id);

  @Query('SELECT * from TUser WHERE name = :name')
  Future<TUser?> findUserByName(String name);

  @Query('DELETE from TUser WHERE id = :id')
  Future<TUser?> deleteUser(int id);

  @insert
  Future<void> insertUser(TUser user);

  @Query(
      'UPDATE TUser SET login = :login, firstName = :firstName, lastName = :lastName, email = :email, address = :address, phone = :phone WHERE id = :id')
  Future<void> updateUser(String login, String firstName, String lastName,
      String email, String address, String phone, int id);
}

@entity
class TUser {
  @PrimaryKey(autoGenerate: true)
  int? id;
  String login;
  String firstName;
  String lastName;
  String email;
  String address;
  String phone;

  TUser(this.id, this.login, this.firstName, this.lastName, this.email,
      this.address, this.phone);

  TUser.fromMap(User user)
      : id = user.id,
        login = user.login,
        firstName = user.firstName,
        lastName = user.lastName,
        email = user.email,
        address = user.address,
        phone = user.phone;

  User get user => User(id, login, firstName, lastName, email, address, phone);
}
