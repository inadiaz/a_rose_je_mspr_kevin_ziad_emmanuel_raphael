// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AppDatabase.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  AdviceDao? _adviceDaoInstance;

  AnnounceDao? _announceDaoInstance;

  PlantDao? _plantDaoInstance;

  PlantAnnounceDao? _plantAnnounceDaoInstance;

  PhotoPlantDao? _photoPlantDaoInstance;

  UserDao? _userDaoInstance;

  UserAdvicePlantDao? _userAdvicePlantDaoInstance;

  UserAnnounceDao? _userAnnounceDaoInstance;

  UserRoleDao? _userRoleDaoInstance;

  Future<sqflite.Database> open(
    String path,
    List<Migration> migrations, [
    Callback? callback,
  ]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `TAdvice` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `title` TEXT NOT NULL, `comment` TEXT NOT NULL, `publishDate` INTEGER NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `TAnnounce` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `title` TEXT NOT NULL, `comment` TEXT NOT NULL, `publishDate` INTEGER NOT NULL, `startDate` INTEGER NOT NULL, `endDate` INTEGER NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `TPlant` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL, `description` TEXT NOT NULL, `photo` TEXT NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `TPlantAnnounce` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `plantId` INTEGER NOT NULL, `announceId` INTEGER NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `TPhotoPlant` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `plantId` INTEGER NOT NULL, `photoId` INTEGER NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `TUser` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `login` TEXT NOT NULL, `firstName` TEXT NOT NULL, `lastName` TEXT NOT NULL, `email` TEXT NOT NULL, `address` TEXT NOT NULL, `phone` TEXT NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `TUserAdvicePlant` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `userId` INTEGER NOT NULL, `adviceId` INTEGER NOT NULL, `plantId` INTEGER NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `TUserAnnounce` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `keeperId` INTEGER NOT NULL, `ownerdId` INTEGER NOT NULL, `annouceId` INTEGER NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `TUserRole` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `type` TEXT NOT NULL)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  AdviceDao get adviceDao {
    return _adviceDaoInstance ??= _$AdviceDao(database, changeListener);
  }

  @override
  AnnounceDao get announceDao {
    return _announceDaoInstance ??= _$AnnounceDao(database, changeListener);
  }

  @override
  PlantDao get plantDao {
    return _plantDaoInstance ??= _$PlantDao(database, changeListener);
  }

  @override
  PlantAnnounceDao get plantAnnounceDao {
    return _plantAnnounceDaoInstance ??=
        _$PlantAnnounceDao(database, changeListener);
  }

  @override
  PhotoPlantDao get photoPlantDao {
    return _photoPlantDaoInstance ??= _$PhotoPlantDao(database, changeListener);
  }

  @override
  UserDao get userDao {
    return _userDaoInstance ??= _$UserDao(database, changeListener);
  }

  @override
  UserAdvicePlantDao get userAdvicePlantDao {
    return _userAdvicePlantDaoInstance ??=
        _$UserAdvicePlantDao(database, changeListener);
  }

  @override
  UserAnnounceDao get userAnnounceDao {
    return _userAnnounceDaoInstance ??=
        _$UserAnnounceDao(database, changeListener);
  }

  @override
  UserRoleDao get userRoleDao {
    return _userRoleDaoInstance ??= _$UserRoleDao(database, changeListener);
  }
}

class _$AdviceDao extends AdviceDao {
  _$AdviceDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _tAdviceInsertionAdapter = InsertionAdapter(
            database,
            'TAdvice',
            (TAdvice item) => <String, Object?>{
                  'id': item.id,
                  'title': item.title,
                  'comment': item.comment,
                  'publishDate': item.publishDate
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<TAdvice> _tAdviceInsertionAdapter;

  @override
  Future<List<TAdvice>> findAllAdvices() async {
    return _queryAdapter.queryList('SELECT * from TAdvice',
        mapper: (Map<String, Object?> row) => TAdvice(
            row['id'] as int?,
            row['title'] as String,
            row['comment'] as String,
            row['publishDate'] as int));
  }

  @override
  Future<TAdvice?> findAdviceById(int id) async {
    return _queryAdapter.query('SELECT * from TAdvice WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TAdvice(
            row['id'] as int?,
            row['title'] as String,
            row['comment'] as String,
            row['publishDate'] as int),
        arguments: [id]);
  }

  @override
  Future<TAdvice?> findAdviceByTitle(String title) async {
    return _queryAdapter.query('SELECT * from TAdvice WHERE title = ?1',
        mapper: (Map<String, Object?> row) => TAdvice(
            row['id'] as int?,
            row['title'] as String,
            row['comment'] as String,
            row['publishDate'] as int),
        arguments: [title]);
  }

  @override
  Future<void> updateAdvice(
    String title,
    String comment,
    int publishDate,
    int id,
  ) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE TAdvice SET title = ?1, comment = ?2, publishDate = ?3 WHERE id = ?4',
        arguments: [title, comment, publishDate, id]);
  }

  @override
  Future<TAdvice?> deleteAdvice(int id) async {
    return _queryAdapter.query('DELETE from TAdvice WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TAdvice(
            row['id'] as int?,
            row['title'] as String,
            row['comment'] as String,
            row['publishDate'] as int),
        arguments: [id]);
  }

  @override
  Future<void> insertAdvice(TAdvice advice) async {
    await _tAdviceInsertionAdapter.insert(advice, OnConflictStrategy.abort);
  }
}

class _$AnnounceDao extends AnnounceDao {
  _$AnnounceDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _tAnnounceInsertionAdapter = InsertionAdapter(
            database,
            'TAnnounce',
            (TAnnounce item) => <String, Object?>{
                  'id': item.id,
                  'title': item.title,
                  'comment': item.comment,
                  'publishDate': item.publishDate,
                  'startDate': item.startDate,
                  'endDate': item.endDate
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<TAnnounce> _tAnnounceInsertionAdapter;

  @override
  Future<List<TAnnounce>> findAllAnnounce() async {
    return _queryAdapter.queryList('SELECT * from TAnnounce',
        mapper: (Map<String, Object?> row) => TAnnounce(
            row['id'] as int?,
            row['title'] as String,
            row['comment'] as String,
            row['publishDate'] as int,
            row['startDate'] as int,
            row['endDate'] as int));
  }

  @override
  Future<TAnnounce?> findAnnounceById(int id) async {
    return _queryAdapter.query('SELECT * from TAnnounce WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TAnnounce(
            row['id'] as int?,
            row['title'] as String,
            row['comment'] as String,
            row['publishDate'] as int,
            row['startDate'] as int,
            row['endDate'] as int),
        arguments: [id]);
  }

  @override
  Future<TAnnounce?> findAnnounceByTitle(String title) async {
    return _queryAdapter.query('SELECT * from TAnnounce WHERE title = ?1',
        mapper: (Map<String, Object?> row) => TAnnounce(
            row['id'] as int?,
            row['title'] as String,
            row['comment'] as String,
            row['publishDate'] as int,
            row['startDate'] as int,
            row['endDate'] as int),
        arguments: [title]);
  }

  @override
  Future<void> updateAnnounce(
    String title,
    String comment,
    int publishDate,
    int startDate,
    int endDate,
    int id,
  ) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE TAnnounce SET title = ?1, comment = ?2, publishDate = ?3, startDate = ?4, endDate = ?5 WHERE id = ?6',
        arguments: [title, comment, publishDate, startDate, endDate, id]);
  }

  @override
  Future<TAnnounce?> deleteAnnounce(int id) async {
    return _queryAdapter.query('DELETE from TAnnounce WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TAnnounce(
            row['id'] as int?,
            row['title'] as String,
            row['comment'] as String,
            row['publishDate'] as int,
            row['startDate'] as int,
            row['endDate'] as int),
        arguments: [id]);
  }

  @override
  Future<void> insertAnnounce(TAnnounce announce) async {
    await _tAnnounceInsertionAdapter.insert(announce, OnConflictStrategy.abort);
  }
}

class _$PlantDao extends PlantDao {
  _$PlantDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _tPlantInsertionAdapter = InsertionAdapter(
            database,
            'TPlant',
            (TPlant item) => <String, Object?>{
                  'id': item.id,
                  'name': item.name,
                  'description': item.description,
                  'photo': item.photo
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<TPlant> _tPlantInsertionAdapter;

  @override
  Future<List<TPlant>> findAllPlant() async {
    return _queryAdapter.queryList('SELECT * from TPlant',
        mapper: (Map<String, Object?> row) => TPlant(
            row['id'] as int?,
            row['name'] as String,
            row['description'] as String,
            row['photo'] as String));
  }

  @override
  Future<TPlant?> findPlantById(int id) async {
    return _queryAdapter.query('SELECT * from TPlant WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TPlant(
            row['id'] as int?,
            row['name'] as String,
            row['description'] as String,
            row['photo'] as String),
        arguments: [id]);
  }

  @override
  Future<TPlant?> findPlantByName(String name) async {
    return _queryAdapter.query('SELECT * from TPlant WHERE name = ?1',
        mapper: (Map<String, Object?> row) => TPlant(
            row['id'] as int?,
            row['name'] as String,
            row['description'] as String,
            row['photo'] as String),
        arguments: [name]);
  }

  @override
  Future<TPlant?> deletePlant(int id) async {
    return _queryAdapter.query('DELETE from TPlant WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TPlant(
            row['id'] as int?,
            row['name'] as String,
            row['description'] as String,
            row['photo'] as String),
        arguments: [id]);
  }

  @override
  Future<void> updatePlant(
    String name,
    String description,
    String photo,
    int id,
  ) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE TPlant SET name = ?1, description = ?2, photo = ?3 WHERE id = ?4',
        arguments: [name, description, photo, id]);
  }

  @override
  Future<void> insertPlant(TPlant plant) async {
    await _tPlantInsertionAdapter.insert(plant, OnConflictStrategy.abort);
  }
}

class _$PlantAnnounceDao extends PlantAnnounceDao {
  _$PlantAnnounceDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _tPlantAnnounceInsertionAdapter = InsertionAdapter(
            database,
            'TPlantAnnounce',
            (TPlantAnnounce item) => <String, Object?>{
                  'id': item.id,
                  'plantId': item.plantId,
                  'announceId': item.announceId
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<TPlantAnnounce> _tPlantAnnounceInsertionAdapter;

  @override
  Future<List<TPlantAnnounce>> findAllPlantAnnounces() async {
    return _queryAdapter.queryList('SELECT * from TPlantAnnounce',
        mapper: (Map<String, Object?> row) => TPlantAnnounce(row['id'] as int?,
            row['plantId'] as int, row['announceId'] as int));
  }

  @override
  Future<TPlantAnnounce?> findPlantAnnounceById(int id) async {
    return _queryAdapter.query('SELECT * from TPlantAnnounce WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TPlantAnnounce(
            row['id'] as int?, row['plantId'] as int, row['announceId'] as int),
        arguments: [id]);
  }

  @override
  Future<TPlantAnnounce?> deletePlantAnnounce(int id) async {
    return _queryAdapter.query('DELETE from TPlantAnnounce WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TPlantAnnounce(
            row['id'] as int?, row['plantId'] as int, row['announceId'] as int),
        arguments: [id]);
  }

  @override
  Future<void> updatePlantAnnounce(
    int plantId,
    int announceId,
    int id,
  ) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE TPlantAnnounce SET plantId = ?1, announceId = ?2 WHERE id = ?3',
        arguments: [plantId, announceId, id]);
  }

  @override
  Future<void> insertPlantAnnounce(TPlantAnnounce plantAnnounce) async {
    await _tPlantAnnounceInsertionAdapter.insert(
        plantAnnounce, OnConflictStrategy.abort);
  }
}

class _$PhotoPlantDao extends PhotoPlantDao {
  _$PhotoPlantDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _tPhotoPlantInsertionAdapter = InsertionAdapter(
            database,
            'TPhotoPlant',
            (TPhotoPlant item) => <String, Object?>{
                  'id': item.id,
                  'plantId': item.plantId,
                  'photoId': item.photoId
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<TPhotoPlant> _tPhotoPlantInsertionAdapter;

  @override
  Future<List<TPhotoPlant>> findAllPhotoPlants() async {
    return _queryAdapter.queryList('SELECT * from TPhotoPlant',
        mapper: (Map<String, Object?> row) => TPhotoPlant(
            row['id'] as int?, row['plantId'] as int, row['photoId'] as int));
  }

  @override
  Future<TPhotoPlant?> findPhotoPlantById(int id) async {
    return _queryAdapter.query('SELECT * from TPhotoPlant WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TPhotoPlant(
            row['id'] as int?, row['plantId'] as int, row['photoId'] as int),
        arguments: [id]);
  }

  @override
  Future<TPhotoPlant?> deletePhotoPlant(int id) async {
    return _queryAdapter.query('DELETE from TPhotoPlant WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TPhotoPlant(
            row['id'] as int?, row['plantId'] as int, row['photoId'] as int),
        arguments: [id]);
  }

  @override
  Future<void> updatePhotoPlant(
    int plantId,
    int photoId,
    int id,
  ) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE TPhotoPlant SET plantId = ?1, photoId = ?2 WHERE id = ?3',
        arguments: [plantId, photoId, id]);
  }

  @override
  Future<void> insertPhotoPlant(TPhotoPlant photoPlant) async {
    await _tPhotoPlantInsertionAdapter.insert(
        photoPlant, OnConflictStrategy.abort);
  }
}

class _$UserDao extends UserDao {
  _$UserDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _tUserInsertionAdapter = InsertionAdapter(
            database,
            'TUser',
            (TUser item) => <String, Object?>{
                  'id': item.id,
                  'login': item.login,
                  'firstName': item.firstName,
                  'lastName': item.lastName,
                  'email': item.email,
                  'address': item.address,
                  'phone': item.phone
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<TUser> _tUserInsertionAdapter;

  @override
  Future<List<TUser>> findAllUsers() async {
    return _queryAdapter.queryList('SELECT * from TUser',
        mapper: (Map<String, Object?> row) => TUser(
            row['id'] as int?,
            row['login'] as String,
            row['firstName'] as String,
            row['lastName'] as String,
            row['email'] as String,
            row['address'] as String,
            row['phone'] as String));
  }

  @override
  Future<TUser?> findUserById(int id) async {
    return _queryAdapter.query('SELECT * from TUser WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TUser(
            row['id'] as int?,
            row['login'] as String,
            row['firstName'] as String,
            row['lastName'] as String,
            row['email'] as String,
            row['address'] as String,
            row['phone'] as String),
        arguments: [id]);
  }

  @override
  Future<TUser?> findUserByName(String name) async {
    return _queryAdapter.query('SELECT * from TUser WHERE name = ?1',
        mapper: (Map<String, Object?> row) => TUser(
            row['id'] as int?,
            row['login'] as String,
            row['firstName'] as String,
            row['lastName'] as String,
            row['email'] as String,
            row['address'] as String,
            row['phone'] as String),
        arguments: [name]);
  }

  @override
  Future<TUser?> deleteUser(int id) async {
    return _queryAdapter.query('DELETE from TUser WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TUser(
            row['id'] as int?,
            row['login'] as String,
            row['firstName'] as String,
            row['lastName'] as String,
            row['email'] as String,
            row['address'] as String,
            row['phone'] as String),
        arguments: [id]);
  }

  @override
  Future<void> updateUser(
    String login,
    String firstName,
    String lastName,
    String email,
    String address,
    String phone,
    int id,
  ) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE TUser SET login = ?1, firstName = ?2, lastName = ?3, email = ?4, address = ?5, phone = ?6 WHERE id = ?7',
        arguments: [login, firstName, lastName, email, address, phone, id]);
  }

  @override
  Future<void> insertUser(TUser user) async {
    await _tUserInsertionAdapter.insert(user, OnConflictStrategy.abort);
  }
}

class _$UserAdvicePlantDao extends UserAdvicePlantDao {
  _$UserAdvicePlantDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _tUserAdvicePlantInsertionAdapter = InsertionAdapter(
            database,
            'TUserAdvicePlant',
            (TUserAdvicePlant item) => <String, Object?>{
                  'id': item.id,
                  'userId': item.userId,
                  'adviceId': item.adviceId,
                  'plantId': item.plantId
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<TUserAdvicePlant> _tUserAdvicePlantInsertionAdapter;

  @override
  Future<List<TUserAdvicePlant>> findAllUserAdvicePlants() async {
    return _queryAdapter.queryList('SELECT * from TUserAdvicePlant',
        mapper: (Map<String, Object?> row) => TUserAdvicePlant(
            row['id'] as int?,
            row['userId'] as int,
            row['adviceId'] as int,
            row['plantId'] as int));
  }

  @override
  Future<TUserAdvicePlant?> findUserAdvicePlantById(int id) async {
    return _queryAdapter.query('SELECT * from TUserAdvicePlant WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TUserAdvicePlant(
            row['id'] as int?,
            row['userId'] as int,
            row['adviceId'] as int,
            row['plantId'] as int),
        arguments: [id]);
  }

  @override
  Future<TUserAdvicePlant?> deleteUserAdvicePlant(int id) async {
    return _queryAdapter.query('DELETE from TUserAdvicePlant WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TUserAdvicePlant(
            row['id'] as int?,
            row['userId'] as int,
            row['adviceId'] as int,
            row['plantId'] as int),
        arguments: [id]);
  }

  @override
  Future<void> updateUserAdvicePlant(
    int userId,
    int adviceId,
    int plantId,
    int id,
  ) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE TUserAdvicePlant SET userId = ?1, adviceId = ?2, plantId = ?3 WHERE id = ?4',
        arguments: [userId, adviceId, plantId, id]);
  }

  @override
  Future<void> insertUserAdvicePlant(TUserAdvicePlant userAdvicePlant) async {
    await _tUserAdvicePlantInsertionAdapter.insert(
        userAdvicePlant, OnConflictStrategy.abort);
  }
}

class _$UserAnnounceDao extends UserAnnounceDao {
  _$UserAnnounceDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _tUserAnnounceInsertionAdapter = InsertionAdapter(
            database,
            'TUserAnnounce',
            (TUserAnnounce item) => <String, Object?>{
                  'id': item.id,
                  'keeperId': item.keeperId,
                  'ownerdId': item.ownerdId,
                  'annouceId': item.annouceId
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<TUserAnnounce> _tUserAnnounceInsertionAdapter;

  @override
  Future<List<TUserAnnounce>> findAllUserAnnounces() async {
    return _queryAdapter.queryList('SELECT * from TUserAnnounce',
        mapper: (Map<String, Object?> row) => TUserAnnounce(
            row['id'] as int?,
            row['keeperId'] as int,
            row['ownerdId'] as int,
            row['annouceId'] as int));
  }

  @override
  Future<TUserAnnounce?> findUserAnnounceById(int id) async {
    return _queryAdapter.query('SELECT * from TUserAnnounce WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TUserAnnounce(
            row['id'] as int?,
            row['keeperId'] as int,
            row['ownerdId'] as int,
            row['annouceId'] as int),
        arguments: [id]);
  }

  @override
  Future<TUserAnnounce?> deleteUserAnnounce(int id) async {
    return _queryAdapter.query('DELETE from TUserAnnounce WHERE id = ?1',
        mapper: (Map<String, Object?> row) => TUserAnnounce(
            row['id'] as int?,
            row['keeperId'] as int,
            row['ownerdId'] as int,
            row['annouceId'] as int),
        arguments: [id]);
  }

  @override
  Future<void> updateUserAnnounce(
    int keeperId,
    int ownerId,
    int announceId,
    int id,
  ) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE TUserAnnounce SET keeperId = ?1, ownerdId = ?2, announceId = ?3 WHERE id = ?4',
        arguments: [keeperId, ownerId, announceId, id]);
  }

  @override
  Future<void> insertUserAnnounce(TUserAnnounce userAnnounce) async {
    await _tUserAnnounceInsertionAdapter.insert(
        userAnnounce, OnConflictStrategy.abort);
  }
}

class _$UserRoleDao extends UserRoleDao {
  _$UserRoleDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _tUserRoleInsertionAdapter = InsertionAdapter(
            database,
            'TUserRole',
            (TUserRole item) =>
                <String, Object?>{'id': item.id, 'type': item.type});

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<TUserRole> _tUserRoleInsertionAdapter;

  @override
  Future<List<TUserRole>> findAllUserRoles() async {
    return _queryAdapter.queryList('SELECT * from TUserRole',
        mapper: (Map<String, Object?> row) =>
            TUserRole(row['id'] as int?, row['type'] as String));
  }

  @override
  Future<TUserRole?> findUserRoleById(int id) async {
    return _queryAdapter.query('SELECT * from TUserRole WHERE id = ?1',
        mapper: (Map<String, Object?> row) =>
            TUserRole(row['id'] as int?, row['type'] as String),
        arguments: [id]);
  }

  @override
  Future<TUserRole?> deleteUserRole(int id) async {
    return _queryAdapter.query('DELETE from TUserRole WHERE id = ?1',
        mapper: (Map<String, Object?> row) =>
            TUserRole(row['id'] as int?, row['type'] as String),
        arguments: [id]);
  }

  @override
  Future<void> updateUserRole(
    String type,
    int id,
  ) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE TUserRole SET type = ?1 WHERE id = ?2',
        arguments: [type, id]);
  }

  @override
  Future<void> insertUserRole(TUserRole userRole) async {
    await _tUserRoleInsertionAdapter.insert(userRole, OnConflictStrategy.abort);
  }
}
