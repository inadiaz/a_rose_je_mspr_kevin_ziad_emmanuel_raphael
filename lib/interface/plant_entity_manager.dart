import 'dart:async';
import '../model/plant.dart';
import 'dart:io' as Io;
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'AppDatabase.dart';

// when a user send an image it's send in asset folder img and erease plant.jpg
String imagePath = '/lib/assets/img/flower.jpeg';

class PlantDataManager {
  Future<PlantDao> get _plantDao async {
    final database =
        await $FloorAppDatabase.databaseBuilder('app_database4.db').build();

    return database.plantDao;
  }

  Future<void> addNewPlant(Plant plant) async {
    final plantDao = await _plantDao;
    return plantDao.insertPlant(TPlant.fromMap(plant));
  }

  Future<void> updatePlantToLocal(Plant plant) async {
    final plantDao = await _plantDao;
    plantDao.updatePlant(plant.name, plant.description, plant.photo, plant.id!);
  }

  Future<void> deleteCurrentPlant(Plant plant) async {
    final plantDao = await _plantDao;
    plantDao.deletePlant(plant.id!);
  }

  Future<Plant?> getSinglePlantByName(String name) async {
    final plantDao = await _plantDao;
    final tPlant = await plantDao.findPlantByName(name);
    return tPlant?.plant;
  }

  Future<Plant?> getSinglePlantById(int id) async {
    final plantDao = await _plantDao;
    final tPlant = await plantDao.findPlantById(id);
    return tPlant?.plant;
  }

  Future<List<Plant>> getPlantList() async {
    final plantDao = await _plantDao;
    final tPlantList = await plantDao.findAllPlant();
    return tPlantList.map((p) => p.plant).toList();
  }
}

@dao
abstract class PlantDao {
  @Query('SELECT * from TPlant')
  Future<List<TPlant>> findAllPlant();

  @Query('SELECT * from TPlant WHERE id = :id')
  Future<TPlant?> findPlantById(int id);

  @Query('SELECT * from TPlant WHERE name = :name')
  Future<TPlant?> findPlantByName(String name);

  @Query('DELETE from TPlant WHERE id = :id')
  Future<TPlant?> deletePlant(int id);

  @insert
  Future<void> insertPlant(TPlant plant);

  @Query(
      'UPDATE TPlant SET name = :name, description = :description, photo = :photo WHERE id = :id')
  Future<void> updatePlant(
      String name, String description, String photo, int id);
}

@entity
class TPlant {
  @PrimaryKey(autoGenerate: true)
  int? id;
  String name;
  String description;
  String photo;

  TPlant(this.id, this.name, this.description, this.photo);

  TPlant.fromMap(Plant plant)
      : id = plant.id,
        name = plant.name,
        description = plant.description,
        photo = plant.photo;

  Plant get plant => Plant(id, name, description, photo);
}
