import 'dart:async';
import 'package:a_rosa_je/interface/AppDatabase.dart';
import '../model/announce.dart';
import 'dart:io' as Io;
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'AppDatabase.dart';

class AnnounceDataManager {
  Future<AnnounceDao> get _announceDao async {
    final database =
        await $FloorAppDatabase.databaseBuilder('app_database4.db').build();
    return database.announceDao;
  }

  Future<void> addNewAnnounce(Announce announce) async {
    final announceDao = await _announceDao;
    return announceDao.insertAnnounce(TAnnounce.fromMap(announce));
  }

  Future<void> updateAnnounceToLocal(Announce announce) async {
    final announceDao = await _announceDao;
    announceDao.updateAnnounce(
        announce.title,
        announce.comment,
        announce.publishDate.microsecondsSinceEpoch,
        announce.startDate.microsecondsSinceEpoch,
        announce.endDate.microsecondsSinceEpoch,
        announce.id!);
  }

  Future<void> deleteCurrentAnnounce(Announce announce) async {
    final announceDao = await _announceDao;
    announceDao.deleteAnnounce(announce.id!);
  }

  Future<Announce?> getSingleAnnounceByTitle(String title) async {
    final announceDao = await _announceDao;
    final tAnnounce = await announceDao.findAnnounceByTitle(title);
    return tAnnounce?.announce;
  }

  Future<Announce?> getSingleAnnounceById(int id) async {
    final announceDao = await _announceDao;
    final tAnnounce = await announceDao.findAnnounceById(id);
    return tAnnounce?.announce;
  }

  Future<List<Announce>> getAnnounceList() async {
    final announceDao = await _announceDao;
    final tAnnounceList = await announceDao.findAllAnnounce();
    return tAnnounceList.map((a) => a.announce).toList();
  }
}

@dao
abstract class AnnounceDao {
  @Query('SELECT * from TAnnounce')
  Future<List<TAnnounce>> findAllAnnounce();

  @Query('SELECT * from TAnnounce WHERE id = :id')
  Future<TAnnounce?> findAnnounceById(int id);

  @Query('SELECT * from TAnnounce WHERE title = :title')
  Future<TAnnounce?> findAnnounceByTitle(String title);

  @insert
  Future<void> insertAnnounce(TAnnounce announce);

  @Query(
      'UPDATE TAnnounce SET title = :title, comment = :comment, publishDate = :publishDate, startDate = :startDate, endDate = :endDate WHERE id = :id')
  Future<void> updateAnnounce(String title, String comment, int publishDate,
      int startDate, int endDate, int id);

  @Query('DELETE from TAnnounce WHERE id = :id')
  Future<TAnnounce?> deleteAnnounce(int id);
}

@entity
class TAnnounce {
  @PrimaryKey(autoGenerate: true)
  int? id;
  String title;
  String comment;
  int publishDate;
  int startDate;
  int endDate;

  TAnnounce(this.id, this.title, this.comment, this.publishDate, this.startDate,
      this.endDate);

  TAnnounce.fromMap(Announce announce)
      : id = announce.id,
        title = announce.title,
        comment = announce.comment,
        publishDate = announce.publishDate.microsecondsSinceEpoch,
        startDate = announce.startDate.microsecondsSinceEpoch,
        endDate = announce.endDate.microsecondsSinceEpoch;

  //Recreate a Announce object with TAnnounce values
  Announce get announce => Announce(
      id,
      title,
      comment,
      DateTime.fromMicrosecondsSinceEpoch(publishDate),
      DateTime.fromMicrosecondsSinceEpoch(startDate),
      DateTime.fromMicrosecondsSinceEpoch(endDate));
}
