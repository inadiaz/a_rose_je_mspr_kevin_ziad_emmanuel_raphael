class Plant {
  final int? _id;
  String _name;
  String _description;
  String _photo;


  Plant(this._id, String name, String description, String photo):
      _name = name,
      _description = description,
      _photo = photo;


  String get description => _description;
  String get name => _name;
  String get photo => _photo;
  int? get id => _id;

  set name(String? name) => _name = name!;
  set description(String? description) => _description = description!;
  set photo(String? photo) => _photo = photo!;

}