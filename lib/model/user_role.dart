class UserRole {
  final int? _id;
  String _type;

  UserRole(this._id, String type) : _type = type;

  int? get id => _id;
  String get type => _type;

  set type(String? type) => _type = type!;
}
