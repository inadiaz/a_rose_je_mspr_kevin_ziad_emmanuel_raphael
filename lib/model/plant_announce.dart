class PlantAnnounce {
  final int? _id;
  int _plantId;
  int _announceId;

  PlantAnnounce(this._id, int plantId, int announceId)
      : _plantId = plantId,
        _announceId = announceId;

  int? get id => _id;
  int get plantId => _plantId;
  int get announceId => _announceId;

  set plantId(int? plantId) => _plantId = plantId!;
  set announceId(int? announceId) => _announceId = announceId!;
}
