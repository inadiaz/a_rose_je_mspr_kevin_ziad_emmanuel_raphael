class User {
  final int? _id;
  String _login;
  String _firstName;
  String _lastName;
  String _email;
  String _address;
  String _phone;

  User(this._id, String login, String firstName, String lastName, String email,
      String address, String phone)
      : _login = login,
        _firstName = firstName,
        _lastName = lastName,
        _email = email,
        _address = address,
        _phone = phone;

  int? get id => _id;
  String get login => _login;
  String get firstName => _firstName;
  String get lastName => _lastName;
  String get email => _email;
  String get address => _address;
  String get phone => _phone;

  set login(String? login) => _login = login!;
  set firstName(String? firstName) => _firstName = firstName!;
  set lastName(String? lastName) => _lastName = lastName!;
  set email(String? email) => _email = email!;
  set address(String? address) => _address = address!;
  set phone(String? phone) => _phone = phone!;
}
