class UserAnnounce {
  final int? _id;
  int _keeperId;
  int _ownerId;
  int _announceId;

  UserAnnounce(this._id, int keeperId, int ownerId, int announceId)
      : _keeperId = keeperId,
        _ownerId = ownerId,
        _announceId = announceId;

  int? get id => _id;
  int get keeperId => _keeperId;
  int get ownerId => _ownerId;
  int get announceId => _announceId;

  set keeperId(int? keeperId) => _keeperId = keeperId!;
  set ownerId(int? ownerId) => _ownerId = ownerId!;
  set announceId(int? announceId) => _announceId = announceId!;
}
