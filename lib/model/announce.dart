import 'dart:core';

class Announce {
  final int? _id;
  String _title;
  String _comment;
  DateTime _publishDate;
  DateTime _startDate;
  DateTime _endDate;

  Announce(this._id, String title, String comment, DateTime publishDate,
      DateTime startDate, DateTime endDate):
    _publishDate = publishDate,
    _title = title,
    _comment = comment,
    _startDate = startDate,
    _endDate = endDate;


  DateTime get endDate => _endDate;
  DateTime get startDate => _startDate;
  DateTime get publishDate => _publishDate;
  String get comment => _comment;
  String get title => _title;
  int? get id => _id;

  set endDate(DateTime value) {
    _endDate = value;
  }

  set startDate(DateTime value) {
    _startDate = value;
  }

  set comment(String value) {
    _comment = value;
  }

  set title(String value) {
    _title = value;
  }
}