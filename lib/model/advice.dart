class Advice {
  final int? _id;
  String _title;
  String _comment;
  DateTime _publishDate;

  Advice(this._id, String title, String comment, DateTime dateTime)
      : _title = title,
        _comment = comment,
        _publishDate = DateTime.now();

  int? get id => _id;

  String get title => _title;

  String get comment => _comment;

  DateTime get publishDate => _publishDate;

  set title(String? title) => _title = title!;

  set comment(String? comment) => _comment = comment!;

  set publishDate(DateTime? publishDate) => _publishDate = publishDate!;
}
