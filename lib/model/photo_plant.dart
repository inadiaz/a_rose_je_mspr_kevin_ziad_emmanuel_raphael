class PhotoPlant {
  final int? _id;
  int _plantId;
  int _photoId;

  PhotoPlant(this._id, int plantId, int photoId)
      : _plantId = plantId,
        _photoId = photoId;

  int? get id => _id;
  int get plantId => _plantId;
  int get photoId => _photoId;

  set plantId(int? plantId) => _plantId = plantId!;
  set photoId(int? photoId) => _photoId = photoId!;
}
