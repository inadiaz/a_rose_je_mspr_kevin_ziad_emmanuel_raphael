class UserAdvicePlant {
  final int? _id;
  int _userId;
  int _adviceId;
  int _plantId;

  UserAdvicePlant(this._id, int userId, int adviceId, int plantId)
      : _userId = userId,
        _adviceId = adviceId,
        _plantId = plantId;

  int? get id => _id;
  int get userId => _userId;
  int get adviceId => _adviceId;
  int get plantId => _plantId;

  set userId(int? userId) => _userId = userId!;
  set adviceId(int? adviceId) => _adviceId = adviceId!;
  set plantId(int? plantId) => _plantId = plantId!;
}
