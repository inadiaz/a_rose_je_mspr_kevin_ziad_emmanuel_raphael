import 'package:a_rosa_je/viewModel/profil_view_model.dart';
import 'package:flutter/material.dart';

import 'Navbar.dart';

abstract class IProfilePageViewModel extends ChangeNotifier {
  String? get pseudo;

  String? get role;

  String? get firstName;

  String? get lastName;

  int? get phone;

  String? get email;

  String? get address;

  String? get image;
}

class ProfilPage extends StatelessWidget {
  //final IProfilePageViewModel _viewModel;
  const ProfilPage(/*this._viewModel,*/ {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const Expanded(flex: 2, child: _TopPortion()),
          Expanded(
            flex: 5,
            child: Padding(
              padding: const EdgeInsets.all(7.0),
              child: Column(
                children: [
                  Text(
                    "Geralt",
                    style: Theme.of(context)
                        .textTheme
                        .headline6
                        ?.copyWith(fontWeight: FontWeight.bold),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 30),
                    child: Column(
                      children: [
                        Card(
                          child: ListTile(
                            leading: Icon(Icons.person),
                            title: Text(" Prenom :"),
                          ),
                        ),
                        Card(
                          child: ListTile(
                            leading: Icon(Icons.person),
                            title: const Text('Nom : '),
                          ),
                        ),
                        Card(
                          child: ListTile(
                            leading: Icon(Icons.work),
                            title: const Text('Role : '),
                          ),
                        ),
                        Card(
                          child: ListTile(
                            leading: Icon(Icons.email),
                            title: const Text('Email'),
                          ),
                        ),
                        Card(
                          child: ListTile(
                            leading: Icon(Icons.phone),
                            title: const Text('Telephone'),
                          ),
                        ),
                        Card(
                          child: ListTile(
                            leading: Icon(Icons.place),
                            title: const Text('Adresse : '),
                          ),
                        ),
                      ],
                    ),
                  ),

/*
                  Row(

                    mainAxisAlignment: MainAxisAlignment.center,


                    children: [
                      ElevatedButton.icon(
                        onPressed: () {},
                        icon: Icon( // <-- Icon
                          Icons.list,
                          size: 24.0,
                        ),
                        label: const Text('Voir annonces'), // <-- Text
                      ),



                    ],
                  ),
*/
                ],
              ),
            ),
          ),
          //   const NavBar()
        ],
      ),
    );
  }
}

class _TopPortion extends StatelessWidget {
  const _TopPortion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      // fit: StackFit.expand,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 80),
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [Color(0xff80DBAA), Color(0xff2D9F62)]),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30),
                bottomRight: Radius.circular(30),
              )),
        ),
        Align(
          alignment: Alignment.center,
          child: SizedBox(
            width: 150,
            height: 150,
            child: Stack(
              fit: StackFit.expand,
              children: [
                Container(
                  decoration: const BoxDecoration(
                    color: Colors.black,
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                            'https://cdn.pixabay.com/photo/2016/03/12/23/18/man-1253004_960_720.jpg'),
                        alignment: Alignment.center),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
