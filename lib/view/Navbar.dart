import 'package:a_rosa_je/view/advices_list_page.dart';
import 'package:a_rosa_je/view/announces_guardian.dart';
import 'package:flutter/material.dart';

class NavBar extends StatefulWidget {
  const NavBar({Key? key}) : super(key: key);

  @override
  State<NavBar> createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  int _selectedIndex=0;
  static NavBar get current => current;



  @override
  Widget build(BuildContext context) {
    return SafeArea(child:
    BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.open_in_new_rounded),
          label: 'Voir Consiels',
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Colors.amber[800],
      onTap: (int index) {
        switch (index) {
          case 0:
          // only scroll to top when current index is selected.
            if (_selectedIndex == index) {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const AnnouncesGuardian()));
            }
            break;
          case 1:
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => const AdvicesListPage()));
            break;
        }
        setState(() {_selectedIndex = index;},
        );
      },
    ),

    );
  }
}
