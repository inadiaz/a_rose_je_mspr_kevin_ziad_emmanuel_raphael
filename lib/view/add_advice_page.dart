import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class AddAdvicePage extends StatefulWidget {
  const AddAdvicePage({super.key});

  @override
  State<AddAdvicePage> createState() => _AddAdvicePageState();
}

class _AddAdvicePageState extends State<AddAdvicePage> {
  final _formKey = GlobalKey<FormState>();
  String _title = "";
  String _comment = "";
  String _plant = "";
  DateTime _date = DateTime.now();

  void _changeTitle(value) {
    setState(() {
      _title = value;
    });
  }

  void _changePlant(value) {
    setState(() {
      _plant = value;
    });
  }

  void _changeComment(value) {
    setState(() {
      _comment = value;
    });
  }

  void _validateFields() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
    }
  }

  @override
  Widget build(BuildContext context) {
    const appName = "A'Rosa_je";
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          appName,
          textAlign: TextAlign.center,
        ),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        const Icon(Icons.title),
                        Expanded(
                          child: TextFormField(
                            decoration: const InputDecoration(
                                helperText: "Entrez titre de l'annonce",
                                hintText: "Titre"),
                            autocorrect: false,
                            textCapitalization: TextCapitalization.words,
                            autofillHints: const [AutofillHints.givenName],
                            keyboardType: TextInputType.name,
                            validator: (value) =>
                                value!.length > 2 ? null : "titre trop court",
                            onSaved: _changeTitle,
                            //onSubmitted: _changeName,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        const Icon(Icons.person),
                        Expanded(
                          child: TextFormField(
                            decoration: const InputDecoration(
                                helperText: "Selectionner la plante",
                                hintText: "Plante"),
                            autocorrect: false,
                            textCapitalization: TextCapitalization.words,
                            autofillHints: const [AutofillHints.givenName],
                            onSaved: _changePlant,
                            //onSubmitted: _changeName,
                          ),
                        ),
                        IconButton(
                            onPressed: () {},
                            icon: const Icon(Icons.add_circle)),
                      ],
                    ),
                    Row(
                      children: [
                        const Icon(Icons.image),
                        IconButton(
                            onPressed: () {},
                            icon: const Icon(Icons.add_a_photo)),
                      ],
                    ),
                    Row(
                      children: [
                        const Icon(Icons.text_fields),
                        Expanded(
                          child: TextFormField(
                            decoration: const InputDecoration(
                                helperText: "Commentaire",
                                hintText: "Lorem ispum"),
                            autocorrect: false,
                            textCapitalization: TextCapitalization.words,
                            autofillHints: const [AutofillHints.givenName],
                            onSaved: _changeComment,
                            //onSubmitted: _changeName,
                          ),
                        ),
                      ],
                    ),
                    ElevatedButton(
                        onPressed: _validateFields,
                        child: const Text("Valider")),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
