import 'package:a_rosa_je/view/add_advice_page.dart';
import 'package:a_rosa_je/view/profil_page.dart';

import 'package:flutter/material.dart';
class DrawerNavigatore extends StatefulWidget {


  const DrawerNavigatore({Key? key}) : super(key: key);

  @override
  State<DrawerNavigatore> createState() => _DrawerNavigatoreState();
}

class _DrawerNavigatoreState extends State<DrawerNavigatore> {

  @override
  Widget build(BuildContext context) {
    return  Container(
      child: Drawer(
        child: ListView(
          children: <Widget> [
            UserAccountsDrawerHeader(
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage("https://cdn.pixabay.com/photo/2016/03/12/23/18/man-1253004_960_720.jpg"),
              ),
              accountName: Text("User"),
              accountEmail: Text("User"),
              decoration:
              BoxDecoration(color: Colors.greenAccent),
            ),ListTile(
              leading: Icon(Icons.person),
              title: Text("Profil"),
              onTap: ()=> Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ProfilPage())),
            ),
            ListTile(
              leading: Icon(Icons.list),
              title: Text("Conseil"),
              onTap: ()=> Navigator.of(context).push(MaterialPageRoute(builder: (context)=>AddAdvicePage())),
            )
          ],
        ),
      ),
    );;
  }
}
