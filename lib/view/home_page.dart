import 'dart:collection';

import 'package:a_rosa_je/view/DrawerNavigatore.dart';
import 'package:a_rosa_je/view/Navbar.dart';
import 'package:a_rosa_je/view/add_announce_page.dart';
import 'package:a_rosa_je/view/advices_list_page.dart';
import 'package:a_rosa_je/view/announces_guardian.dart';
import 'package:a_rosa_je/view/profil_page.dart';
import 'package:a_rosa_je/viewModel/home_view_model.dart';
import 'package:flutter/material.dart';

abstract class IHomeViewModel extends ChangeNotifier {
  //put all ascesseur and methode of view model we need in the view
}

List<String> values = ["plante", "plante2", "plante3", "plante4"];
Map<String, String> test = {
  "Plante": "link",
  "Plante1": "link1",
  "Plante22": "link2",
  "Plante3": "link3"
};

class HomePage extends StatefulWidget {
  //si on crée des tests utilisier une interface du view model et faire que le view model herite de l'interface
  final IHomeViewModel _viewModel;
  const HomePage(this._viewModel, {super.key});

  //annimatedBuilder(annimation: _viewModel ,builder: (context, child)
  // _viewModel.object  return {Column(...la ou est l'objet view model)}

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  var runtimeTypeName = (HomePage).toString();


  @override
  Widget build(BuildContext context) {
    const appName = "Accueil";
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          appName,
        ),centerTitle: true,

      ),
      drawer: DrawerNavigatore(),
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                width: double.infinity,
                // height: MediaQuery.of(context).size.height*0.7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Padding(
                      padding: EdgeInsets.all(15),
                      child: Text(
                        'Rechercher sur les fleurs',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                            color: Colors.black54),
                      ),
                    ),
                    TextField(
                      style: const TextStyle(
                        color: Colors.black,
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: const Color(0xff9FE5BF),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.1),
                              borderSide: BorderSide.none),
                          hintText: "ex: Achillée ",
                          prefixIcon: const Icon(Icons.search),
                          prefixIconColor: const Color(0xff279057)),
                    )
                  ],
                ),
              ),
              Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    OutlinedButton(
                        onPressed: () {}, child: const Text("Mes annonces")),
                    OutlinedButton(
                        onPressed: () {}, child: const Text("Annonces Global")),
                  ],
                ),
              ),
              Expanded(
                  child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 15),
                child: ListView.builder(
                    itemCount: values.length, itemBuilder: _streamList),
              )),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Ajouter une annonce",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                  IconButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const AddAnnouncePage()));
                      },
                      icon: const Icon(Icons.add_circle)),

                ],
              ),
             const NavBar()
            ],
          ),
        ),
      ),

    );
  }

  Widget _streamList(BuildContext context, int rowNumber) {
    final result = values[rowNumber];
    return Card(
      color: Color.fromRGBO(158, 227, 190, 2.0),
      margin: const EdgeInsets.symmetric(vertical: 5),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: ListTile(

          leading: FlutterLogo(size: 72.0),
          //Image.network(
            //'test',width: 10,height: 10,
        //  ),
          title: Text(result),
        ),
      ),
    );
  }
}
