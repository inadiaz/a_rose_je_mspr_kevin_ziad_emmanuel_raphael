import 'package:a_rosa_je/view/DrawerNavigatore.dart';
import 'package:a_rosa_je/view/Navbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class AnnouncesGuardian extends StatefulWidget {
  const AnnouncesGuardian({super.key});

  @override
  State<AnnouncesGuardian> createState() => _AnnouncesGuardianState();
}

class _AnnouncesGuardianState extends State<AnnouncesGuardian> {
  @override
  Widget build(BuildContext context) {
    const appName = "Gardinnage";
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            appName,

          ),
          centerTitle: true,

        ),drawer: DrawerNavigatore(),
        body: SafeArea(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
              Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                IconButton(onPressed: () {}, icon: const Icon(Icons.search)),
              ]),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  OutlinedButton(
                      onPressed: () {}, child: const Text("Mes engagements")),
                  const Text(" | "),
                  OutlinedButton(
                      onPressed: () {}, child: const Text("Annonces Global")),
                ],
              ),
              // Todo here put gps API
              const Text("GPS API"),
              Spacer(),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                ElevatedButton(
                    onPressed: () {}, child: const Text("Valider annonce")),
                //Flex(direction: ),
                ElevatedButton(
                    onPressed: () {}, child: const Text("Gardiennage en Cour")),
              ]),
                  NavBar()
            ]
            )
        )
    );
  }
}
