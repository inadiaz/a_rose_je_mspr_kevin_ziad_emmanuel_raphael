import 'package:a_rosa_je/view/single_advice_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class AdvicesListPage extends StatefulWidget {
  const AdvicesListPage({super.key});

  @override
  State<AdvicesListPage> createState() => _AdvicesListPageState();
}

List<String> values = ["plante", "plante2", "plante3", "plante"];

class _AdvicesListPageState extends State<AdvicesListPage> {
  @override
  Widget build(BuildContext context) {
    const appName = "A'Rosa_je";
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          appName,
          textAlign: TextAlign.center,
        ),
      ),
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                children: [
                  IconButton(onPressed: () {}, icon: const Icon(Icons.search)),
                ],
              ),
              Expanded(
                  child: ListView.builder(
                      itemCount: values.length, itemBuilder: _streamList)),
            ],
          ),
        ),
      ),
    );
  }

  Widget _streamList(BuildContext context, int index) {
    final result = values[index];
    return Container(
        padding: const EdgeInsets.fromLTRB(10, 30, 10, 10),
        height: 230,
        width: double.maxFinite,
        child: Card(
            elevation: 5,
            child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: 140,
                          height: 100,
                          child: Image.network("https://cdn.pixabay.com/photo/"
                              "2012/03/01/00/55/flowers-19830_640.jpg"),
                        ),
                        Flexible(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: 100,
                                margin: const EdgeInsets.fromLTRB(5, 5, 55, 0),
                                padding: const EdgeInsets.all(5),
                                child: OutlinedButton(
                                  onPressed: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const SingleAdvicePage()));
                                  },
                                  child: Text(result,
                                      style: TextStyle(fontSize: 12)),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                            margin: EdgeInsets.all(10),
                            width: 150,
                            height: 50,
                            child: Text(
                              'Dozens of turbulences willt of a modern advice, evacuate the ionic cannon!.',
                              overflow: TextOverflow.visible,
                              textAlign: TextAlign.justify,
                            )),
                      ],
                    )
                  ],
                ))));
  }
}
