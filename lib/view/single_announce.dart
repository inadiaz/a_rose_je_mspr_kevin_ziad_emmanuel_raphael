import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:a_rosa_je/view/advices_list_page.dart';

class SingleAnnouncePage extends StatefulWidget {
  const SingleAnnouncePage({super.key});

  @override
  State<SingleAnnouncePage> createState() => _SingleAnnouncePageState();
}

class _SingleAnnouncePageState extends State<SingleAnnouncePage> {
  @override
  Widget build(BuildContext context) {
    const appName = "A'Rosa_je";
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          appName,
          textAlign: TextAlign.center,
        ),
      ),
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Image(
              image: AssetImage(
                  '/lib/asset/images/800_400_11-768x384-3841391776.jpeg')),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                "Orchidée",
                style: Theme.of(context).textTheme.headline5,
              )
            ],
          ),
          const Text("description de l'annonce"),
          // Todo here put gps API
          const Text("GPS API"),
          ElevatedButton(onPressed: () {}, child: const Text("Echanger"))
        ],
      )),
    );
  }
}
