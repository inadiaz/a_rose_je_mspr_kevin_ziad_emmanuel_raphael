import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';


class SingleAdvicePage extends StatelessWidget {
  const SingleAdvicePage({super.key});

  @override
  Widget build(BuildContext context) {
    const appName = "A'Rosa_je";
    var color_txt=const Color(0xFF121B1C);
    Color c1 = const Color(0xFFE4E6EA);
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          appName,
          textAlign: TextAlign.center,
        ),
      ),
      body: SafeArea(

          child: Padding(
            padding: const EdgeInsets.all(13.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
               Card(
                 elevation: 15,
                 child: Container(
                      child: Image.network("https://cdn.pixabay.com/photo/2012/03/01/00/55/flowers-19830_640.jpg"),
                  ),
               ),
                SizedBox(height: 20),
                Row(

                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("Orchidée",style: GoogleFonts.poppins(
                      color: color_txt=const Color(0xFF427577),
                      fontSize: 16,
                      fontWeight: FontWeight.w600


                    ),

                    )
                  ],
                ),
                SizedBox(height: 10,),
                Card(
                  elevation: 15,
                  child: Container(

                    padding: EdgeInsets.all(10),

                    color:c1 ,
                      child: const Text("is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic",
                        style: TextStyle(
                          fontSize: 15,
                        fontWeight: FontWeight.w500,
                        height: 2.1,

                      ),textAlign: TextAlign.justify,)),
                )
              ],
            ),
          ),
        ),

    );
  }
}
