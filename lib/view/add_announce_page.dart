import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';


class AddAnnouncePage extends StatefulWidget {
  const AddAnnouncePage({super.key});

  @override
  State<AddAnnouncePage> createState() => _AddAnnouncePageState();
}

class _AddAnnouncePageState extends State<AddAnnouncePage> {
  final _formKey = GlobalKey<FormState>();
  String _title = "";
  String _comment = "";
  String _plant = "";
  DateTime _date = DateTime.now();

 var _seletectedDate= TextEditingController();


  DateTime _dateTime= DateTime.now();
  _selectedDate(BuildContext context) async{
    var pickDate= await showDatePicker(
        context: context,
        initialDate: _dateTime,
        firstDate: DateTime(1900),
        lastDate: DateTime(2100)
    );
    if (pickDate != null){
      setState(() {
        _seletectedDate.text=  DateFormat('dd/MM/yyyy').format(pickDate);
        _dateTime=pickDate;
      }
      );

    }
  }





  void _changeTitle(value) {
    setState(() {
      _title = value;
    });
  }

  void _changePlant(value) {
    setState(() {
      _plant = value;
    });
  }

  void _changeComment(value) {
    setState(() {
      _comment = value;
    });
  }

  void _changeDate(value) {
    setState(() {
      _date = value;
    });
  }

  void _validateFields() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
    }
  }

  @override
  Widget build(BuildContext context) {
    const appName = "A'Rosa_je";
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          appName,
          textAlign: TextAlign.center,
        ),
      ),


      body: SingleChildScrollView(

        child: Container(
          padding: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
          decoration: BoxDecoration(


            border: Border.all(color: Colors.black12),




          ),

          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 50),
          child: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          children: [
                            const Icon(Icons.title),
                            Expanded(
                              child: TextFormField(

                                decoration: const InputDecoration(
                                    helperText: "Entrez titre de l'annonce",
                                    hintText: "Titre"),
                                autocorrect: false,
                                textCapitalization: TextCapitalization.words,
                                autofillHints: const [AutofillHints.givenName],
                                keyboardType: TextInputType.name,
                                validator: (value) =>
                                    value!.length > 2 ? null : "titre trop court",
                                onSaved: _changeTitle,
                                //onSubmitted: _changeName,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            const Icon(Icons.person),
                            Expanded(
                              child: TextFormField(
                                decoration: const InputDecoration(
                                    helperText: "Selectionner la plante",
                                    hintText: "Plante"),
                                autocorrect: false,
                                textCapitalization: TextCapitalization.words,
                                autofillHints: const [AutofillHints.givenName],
                                onSaved: _changePlant,
                                //onSubmitted: _changeName,
                              ),
                            ),
                          ],
                        ),

                            Material(
                              child: TextField(
                              controller: _seletectedDate,

                                decoration: InputDecoration(
                                    labelText: 'Date',
                                    hintText: 'Saisir une date' ,
                                    prefixIcon: InkWell(
                                      onTap: () async {
                                       _selectedDate(context);
                                      },
                                      child: const Icon(Icons.calendar_today),
                                    )
                                ) ,
                              ),
                            ),

                        Row(
                          children: [
                            const Icon(Icons.text_fields),
                            Expanded(
                              child: TextFormField(
                                decoration: const InputDecoration(
                                    helperText: "Commentaire",
                                    hintText: "Lorem ispum"),
                                autocorrect: false,
                                textCapitalization: TextCapitalization.words,
                                autofillHints: const [AutofillHints.givenName],
                                onSaved: _changeComment,
                                //onSubmitted: _changeName,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 20),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            shape: StadiumBorder(),

                            padding: EdgeInsets.symmetric(
                              horizontal: 100,
                              vertical: 8,
                            ),
                          ),
                          child: Text(
                            'Envoyer',
                            style: GoogleFonts.poppins(
                              color: Colors.white,
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          onPressed: () {
                          },
                        ),
                      ],
                    )
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}


// for date picker 
//               Center(
            //     child: TextField(
            //   readOnly: true,
            //   controller: dateController,
            //   decoration: const InputDecoration(hintText: 'Pick your Date'),
            //   onTap: () async {
            //     var date = await showDatePicker(
            //         context: context,
            //         initialDate: DateTime.now(),
            //         firstDate: DateTime(1900),
            //         lastDate: DateTime(2100));
            //     if (date != null) {
            //       dateController.text = DateFormat('MM/dd/yyyy').format(date);
            //     }
            //   },
            // ))
