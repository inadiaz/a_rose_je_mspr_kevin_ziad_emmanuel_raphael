import 'package:a_rosa_je/model/plant.dart';
import '../interface/plant_entity_manager.dart';

class PlantManager {
  Plant? _currentPlant;
  List<Plant> _plants = [];
  final PlantDataManager _plantDataManager = PlantDataManager();

  Future<List<Plant>> loadPlantListFromLocal() async {
    _plants = await _plantDataManager.getPlantList();
    return _plants;
  }

  Future<Plant?> loadSinglePlantFromLocal(Plant plant) async {
    _currentPlant = await _plantDataManager.getSinglePlantById(plant.id!);
    return _currentPlant;
  }

  Future<Plant?> searchPlantByNameFromLocal(String plantName) async {
    _currentPlant = await _plantDataManager.getSinglePlantByName(plantName);
    return _currentPlant;
  }

  void addPlantToLocal(
      String plantName, String plantDescription, String plantPhoto) {
    final plant = Plant(null, plantName, plantDescription, plantPhoto);
    //_plants.add(plant);
    _plantDataManager.addNewPlant(plant);
  }

  void changePlantToLocal(Plant plant, String plantName,
      String plantDescription, String plantPhoto) {
    final plantToSaved =
        Plant(plant.id, plantName, plantDescription, plantPhoto);
    _plantDataManager.updatePlantToLocal(plantToSaved);
  }

  void deletePlantToLocal(plant) {
    _plantDataManager.deleteCurrentPlant(plant);
  }

  List<Plant> get plants {
    return _plants;
  }

  Plant? get currentPlant => _currentPlant;
}
