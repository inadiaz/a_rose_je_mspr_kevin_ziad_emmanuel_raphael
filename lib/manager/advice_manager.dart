import 'package:a_rosa_je/model/advice.dart';
import 'package:a_rosa_je/interface/advice_entity_manager.dart';

class AdviceManager {
  Advice? _currentAdvice;
  List<Advice> _advices = [];
  final AdviceDataManager _adviceDataManager = AdviceDataManager();

  Future<List<Advice>> loadAdviceListFromLocal() async {
    _advices = await _adviceDataManager.getAdviceList();
    return _advices;
  }

  Future<Advice?> loadSingleAdviceFromLocal(Advice advice) async {
    _currentAdvice = await _adviceDataManager.getSingleAdviceById(advice.id!);
    return _currentAdvice;
  }

  Future<Advice?> searchAdviceByNameFromLocal(String adviceTitle) async {
    _currentAdvice =
        await _adviceDataManager.getSingleAdviceByTitle(adviceTitle);
    return _currentAdvice;
  }

  void addAdviceToLocal(Advice advice, String adviceTitle, String adviceComment,
      DateTime advicePublishDate) {
    final advice = Advice(null, adviceTitle, adviceComment, advicePublishDate);
    _adviceDataManager.addNewAdvice(advice);
  }

  void changeAdviceToLocal(Advice advice, String adviceTitle,
      String adviceComment, DateTime advicePublishDate) {
    final adviceToSaved =
        Advice(advice.id, adviceTitle, adviceComment, advicePublishDate);
    _adviceDataManager.updateAdviceToLocal(adviceToSaved);
  }

  void deleteAdviceToLocal(advice) {
    _adviceDataManager.deleteCurrentAdvice(advice);
  }

  List<Advice> get advices {
    return _advices;
  }

  Advice? get currentAdvice => _currentAdvice;
}
