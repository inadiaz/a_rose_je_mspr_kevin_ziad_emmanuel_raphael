import 'package:a_rosa_je/model/user.dart';
import 'package:a_rosa_je/interface/user_entity_manager.dart';

class UserManager {
  User? _currentUser;
  List<User> _users = [];
  final UserDataManager _userDataManager = UserDataManager();

  Future<List<User>> loadUserListFromLocal() async {
    _users = await _userDataManager.getUserList();
    return _users;
  }

  Future<User?> loadSingleUserFromLocal(int userId) async {
    _currentUser = await _userDataManager.getSingleUserById(userId);
    return _currentUser;
  }

  Future<User?> searchUserByNameFromLocal(String userName) async {
    _currentUser = await _userDataManager.getSingleUserByName(userName);
    return _currentUser;
  }

  void addUserToLocal(
      String userLogin,
      String userFirstName,
      String userLastName,
      String userEmail,
      String userAddress,
      String userPhone) {
    final user = User(null, userLogin, userFirstName, userLastName, userEmail,
        userAddress, userPhone);
    _userDataManager.addNewUser(user);
  }

  void changeUserToLocal(
      int userId,
      String userLogin,
      String userFirstName,
      String userLastName,
      String userEmail,
      String userAddress,
      String userPhone) {
    final userToSaved = User(userId, userLogin, userFirstName, userLastName,
        userEmail, userAddress, userPhone);
    _userDataManager.updateUserToLocal(userToSaved);
  }

  void deleteUserToLocal(user) {
    _userDataManager.deleteCurrentUser(user);
  }

  List<User> get plants {
    return _users;
  }

  User? get currentUser => _currentUser;
}
