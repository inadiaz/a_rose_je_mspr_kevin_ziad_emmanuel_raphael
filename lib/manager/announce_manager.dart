import 'package:a_rosa_je/model/announce.dart';

import '../interface/announce_entity_manager.dart';

class AnnounceManager {
  Announce? _currentAnnounce;
  List<Announce> _announces = [];
  final AnnounceDataManager _announceDataManager = AnnounceDataManager();

  Future<List<Announce>> loadAnnounceListFromLocal() async {
    _announces = await _announceDataManager.getAnnounceList();
    return _announces;
  }

  Future<Announce?> loadSingleAnnounceFromLocal(Announce announce) async {
    _currentAnnounce =
        await _announceDataManager.getSingleAnnounceById(announce.id!);
    return _currentAnnounce;
  }

  Future<Announce?> searchAnnounceByTitleFromLocal(String announceTitle) async {
    _currentAnnounce =
        await _announceDataManager.getSingleAnnounceByTitle(announceTitle);
    return _currentAnnounce;
  }

  void addAnnounceToLocal(String announceTitle, String announceComment,
      DateTime announcePublishDate, DateTime startDate, DateTime endDate) {
    final announce = Announce(null, announceTitle, announceComment,
        announcePublishDate, startDate, endDate);
    _announceDataManager.addNewAnnounce(announce);
  }

  void changePlantToLocal(
      Announce announce,
      String announceTitle,
      String announceComment,
      DateTime announcePublishDate,
      DateTime startDate,
      DateTime endDate) {
    final announceToSaved = Announce(announce.id, announceTitle,
        announceComment, announcePublishDate, startDate, endDate);
    _announceDataManager.updateAnnounceToLocal(announceToSaved);
  }

  void deleteAnnounceToLocal(announce) {
    _announceDataManager.deleteCurrentAnnounce(announce);
  }

  List<Announce> get anounces {
    return _announces;
  }
}
